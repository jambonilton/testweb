package org.nps;

import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

public class TestWeb {

    public static final int PORT = 8080;

    public static void main(final String[] args) throws Exception {
        System.out.println("Listening on port "+PORT+"...");
        Undertow server = Undertow.builder()
                .addHttpListener(PORT, "localhost")
                .setHandler(new HttpHandler() {
                    public void handleRequest(final HttpServerExchange exchange) throws Exception {
                        exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/plain");
                        exchange.getResponseSender().send("Hello World");
                    }
                }).build();
        server.start();
    }

}
