#!/usr/bin/env bash
docker run -d --name es1 elasticsearch
docker run -d --name cs1 cassandra
docker run -d -P --name mytitan --link es1:elasticsearch --link cs1:cassandra apobbati/titan-rexster
echo "Titan DB Online!"