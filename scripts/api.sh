#!/usr/bin/env bash
JOB=$(docker run -d -p 8080 -v $(pwd)/target:/opt/api java java -jar /opt/api/testweb-1.0-SNAPSHOT-jar-with-dependencies.jar)
PORT=$(docker port $JOB 8080 | awk -F: '{ print $2 }')
echo "Java API job $JOB running on port $PORT"
curl "localhost:$PORT"